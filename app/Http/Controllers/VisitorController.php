<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;

class VisitorController extends Controller
{
    public function visitors()
    {
        $user = Sentinel::getUser();
       // dd($user);
       return view('visitor.user_detail',['user'=>$user]);
       
    }
}
