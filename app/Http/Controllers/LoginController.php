<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel ;

class LoginController extends Controller
{
    //
    public function login()
    {
        return view('authentication.login');
    }

    public function postLogin(Request $request)
    {
        $user = Sentinel::authenticate($request->all());
        // dd($user);
        if($user){
            $slug = Sentinel::getUser()->roles()->first()->slug;
           //
            if( $slug == 'admin' ){
                return redirect('/earnings');
            }
            elseif($slug == 'manager' )
            {
                return redirect('/tasks');
            }
           
        }
        else{
            return redirect('/');
        }
        
    }
    public function logout()
    {
        Sentinel::logout();

        return redirect('/login');
    }
}
