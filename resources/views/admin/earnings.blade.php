@extends('layouts.master');

@section('content');
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Admin</h3>
                </div>
                <div class="panel-body">
                    Total Earnings 99999

                    <form action="/logout"  method="POST" id="logout-form">
                                {{ csrf_field() }}
                                <a href="#" onclick="document.getElementById('logout-form').submit()">Logout</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection