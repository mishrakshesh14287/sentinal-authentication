<div class="header clearfix">
<nav>
    <ul class="nav nav-pills pull-right">
    @if(Sentinel::check())
    <li role="presentation">
        <form action="/logout"  method="POST" id="logout-form">
            {{ csrf_field() }}
            <a href="#" onclick="document.getElementById('logout-form').submit()">Logout</a>
        </form>
    </li>
    <li role="presentation"><a href="{{route('fileUpload')}}">Upload Image</a></li>
       
    @else
        <li role="presentation"><a href="{{route('login')}}">Login</a></li>
        <li role="presentation"><a href="{{route('register')}}">Register</a></li>
    @endif
    </ul>
</nav>
<h3 class="text-muted">
@if(Sentinel::check())
    hello, {{ Sentinel::getUser()->first_name }}
@else
   Authentication With Sentinel
@endif
</h3>
</div>