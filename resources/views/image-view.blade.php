
@extends('layouts.master')
@section('content')


    <div class="container">
        @if ($success)
            <div class="alert alert-success">
                <ul>
                     <li>{{ $success }}</li>
                    
                </ul>
            </div>
        @endif
 
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Image
                    </th>               
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img src="<?php echo $img_path; ?>"></td>
                  
                </tr>
            </tbody>        
        </table>       
    </div>
@endsection