-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2019 at 09:17 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sentinel`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'NUsu2KxqEuz88YeqQhnG51Mo0HdvjY4m', 1, '2019-03-13 22:24:20', '2019-03-13 22:24:20', '2019-03-13 22:24:20'),
(2, 2, 'u1NrASNUjl1pSkoKBcjpkYP4vdx6WeoO', 0, NULL, '2019-03-14 01:36:29', '2019-03-14 01:36:29'),
(3, 3, 'eEF59CtoaxaFC6Gsrpllo6a4sshDekbY', 1, '2019-03-14 01:49:55', '2019-03-14 01:49:55', '2019-03-14 01:49:55'),
(4, 4, 'HfIYeoekO19JAgZHJyG5YIp2AIE6yjWF', 1, '2019-03-14 01:52:47', '2019-03-14 01:52:47', '2019-03-14 01:52:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_07_02_230147_migration_cartalyst_sentinel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'V3CCoM4qBxlCcyj2ut9QbMLVTvcGrwpo', '2019-03-13 22:44:00', '2019-03-13 22:44:00'),
(6, 1, 'TGkdG3dfI0FR8to9c8OGVTFgHprdTMA7', '2019-03-14 01:18:08', '2019-03-14 01:18:08'),
(8, 1, 'VmomQsVqQQQqzETaQz9VlxdD7cAKWVEN', '2019-03-14 02:01:58', '2019-03-14 02:01:58'),
(9, 1, 'e03cz2K9JNhJMZ3mtzWhVRyGGuIUw3Rk', '2019-03-14 02:02:25', '2019-03-14 02:02:25'),
(10, 1, 'fUwu4OZSxov5ZcH7dcn6tpQLY5sSKLoF', '2019-03-14 02:02:52', '2019-03-14 02:02:52'),
(13, 4, 'WMZ5HOgeFRbx5Ixk3oJpYLbUVjy453Ne', '2019-03-14 02:19:59', '2019-03-14 02:19:59'),
(16, 4, '8q8Vg1nikUwRY44PLzbOyQP4pjjd4j8r', '2019-03-14 02:36:03', '2019-03-14 02:36:03'),
(17, 4, 'URQV5fwdNfjegEHQbo7juBv6Mgd74GgT', '2019-03-14 02:38:31', '2019-03-14 02:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, NULL, NULL),
(2, 'manager', 'Manager', NULL, NULL, NULL),
(3, 'visitor', 'Visitor', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-03-13 18:30:00', '2019-03-13 18:30:00'),
(2, 2, '2019-03-14 01:36:29', '2019-03-14 01:36:29'),
(4, 3, '2019-03-14 01:52:47', '2019-03-14 01:52:47');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2019-03-13 23:54:29', '2019-03-13 23:54:29'),
(2, NULL, 'ip', '127.0.0.1', '2019-03-13 23:54:30', '2019-03-13 23:54:30'),
(3, NULL, 'global', NULL, '2019-03-13 23:56:38', '2019-03-13 23:56:38'),
(4, NULL, 'ip', '127.0.0.1', '2019-03-13 23:56:38', '2019-03-13 23:56:38'),
(5, NULL, 'global', NULL, '2019-03-14 01:53:47', '2019-03-14 01:53:47'),
(6, NULL, 'ip', '127.0.0.1', '2019-03-14 01:53:47', '2019-03-14 01:53:47'),
(7, NULL, 'global', NULL, '2019-03-14 01:54:17', '2019-03-14 01:54:17'),
(8, NULL, 'ip', '127.0.0.1', '2019-03-14 01:54:17', '2019-03-14 01:54:17'),
(9, NULL, 'global', NULL, '2019-03-14 02:00:22', '2019-03-14 02:00:22'),
(10, NULL, 'ip', '127.0.0.1', '2019-03-14 02:00:22', '2019-03-14 02:00:22'),
(11, NULL, 'global', NULL, '2019-03-14 02:17:11', '2019-03-14 02:17:11'),
(12, NULL, 'ip', '127.0.0.1', '2019-03-14 02:17:11', '2019-03-14 02:17:11'),
(13, NULL, 'global', NULL, '2019-03-14 02:18:17', '2019-03-14 02:18:17'),
(14, NULL, 'ip', '127.0.0.1', '2019-03-14 02:18:17', '2019-03-14 02:18:17'),
(15, NULL, 'global', NULL, '2019-03-14 02:18:46', '2019-03-14 02:18:46'),
(16, NULL, 'ip', '127.0.0.1', '2019-03-14 02:18:46', '2019-03-14 02:18:46'),
(17, NULL, 'global', NULL, '2019-03-14 02:19:24', '2019-03-14 02:19:24'),
(18, NULL, 'ip', '127.0.0.1', '2019-03-14 02:19:25', '2019-03-14 02:19:25'),
(19, NULL, 'global', NULL, '2019-03-14 02:20:30', '2019-03-14 02:20:30'),
(20, NULL, 'ip', '127.0.0.1', '2019-03-14 02:20:30', '2019-03-14 02:20:30'),
(21, 4, 'user', NULL, '2019-03-14 02:20:30', '2019-03-14 02:20:30'),
(22, NULL, 'global', NULL, '2019-03-14 02:37:06', '2019-03-14 02:37:06'),
(23, NULL, 'ip', '127.0.0.1', '2019-03-14 02:37:07', '2019-03-14 02:37:07'),
(24, 4, 'user', NULL, '2019-03-14 02:37:07', '2019-03-14 02:37:07'),
(25, NULL, 'global', NULL, '2019-03-14 02:37:35', '2019-03-14 02:37:35'),
(26, NULL, 'ip', '127.0.0.1', '2019-03-14 02:37:35', '2019-03-14 02:37:35'),
(27, 4, 'user', NULL, '2019-03-14 02:37:35', '2019-03-14 02:37:35'),
(28, NULL, 'global', NULL, '2019-03-14 02:38:23', '2019-03-14 02:38:23'),
(29, NULL, 'ip', '127.0.0.1', '2019-03-14 02:38:23', '2019-03-14 02:38:23'),
(30, 4, 'user', NULL, '2019-03-14 02:38:23', '2019-03-14 02:38:23'),
(31, NULL, 'global', NULL, '2019-03-14 02:39:25', '2019-03-14 02:39:25'),
(32, NULL, 'ip', '127.0.0.1', '2019-03-14 02:39:25', '2019-03-14 02:39:25'),
(33, 4, 'user', NULL, '2019-03-14 02:39:25', '2019-03-14 02:39:25'),
(34, NULL, 'global', NULL, '2019-03-14 02:39:50', '2019-03-14 02:39:50'),
(35, NULL, 'ip', '127.0.0.1', '2019-03-14 02:39:50', '2019-03-14 02:39:50'),
(36, 4, 'user', NULL, '2019-03-14 02:39:50', '2019-03-14 02:39:50'),
(37, NULL, 'global', NULL, '2019-03-14 02:41:07', '2019-03-14 02:41:07'),
(38, NULL, 'ip', '127.0.0.1', '2019-03-14 02:41:07', '2019-03-14 02:41:07'),
(39, 4, 'user', NULL, '2019-03-14 02:41:07', '2019-03-14 02:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `location`, `created_at`, `updated_at`) VALUES
(1, 'mishrakshesh14287@gmail.com', '$2y$10$CGqPNqtkLt/fswNK7t4gZOIk/7gB0D5J2skTnQfezaEwVhymWd42K', NULL, '2019-03-14 02:33:14', 'Shesh', 'Mishra', 'Kanpur', '2019-03-13 22:24:20', '2019-03-14 02:33:14'),
(2, 'test@gmail.com', '$2y$10$FQsWVp5h6fvLk1o68kd7muv4Vbg2KuR7BdrWzAToYYe/4hld.mNta', NULL, NULL, 'test123', 'mishra', 'unnao', '2019-03-14 01:36:29', '2019-03-14 01:36:29'),
(3, 'testshesh@gmail.com', '$2y$10$llSMWzV/KXuKGGdj8h5uE.GmIk1t44fl0zKtwIZgv43F9JkvzEEt.', NULL, NULL, 'sheshvistor', NULL, 'lko', '2019-03-14 01:49:55', '2019-03-14 01:49:55'),
(4, 'testvisitors@gmail.com', '$2y$10$b55rJM8E0tLX7GwKyMKy/O6H1pBJnYAY7/xWsAyVAjqTfRjKd7BRu', NULL, '2019-03-14 02:38:31', 'test11', NULL, 'lko', '2019-03-14 01:52:47', '2019-03-14 02:38:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
