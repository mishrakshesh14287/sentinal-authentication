<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    //return view('welcome');
    return view('authentication.login');
});

Route::group(['middleware'=>'visitors'],function(){
    Route::get('/register','RegistrationController@register')->name('register');

    Route::post('/register','RegistrationController@postRegister');

    Route::get('/login','LoginController@login')->name('login');

    Route::post('/login','LoginController@postLogin');
    
    
});


Route::post('/logout','LoginController@logout');

Route::get('/earnings','AdminController@earnings')->middleware('admin');

Route::get('/tasks','ManagerController@tasks')->middleware('manager');

Route::group(['middleware' => 'web'], function () {
        Route::get('fileUpload', function () {

            return view('fileUpload');

        })->name('fileUpload');

        Route::post('fileUpload', ['as'=>'fileUpload','uses'=>'HomeController@fileUpload']);
});
